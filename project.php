<?php 
/*
Template Name: Project List
*/
get_header(); ?>

<div id="content">

<div class="postsbody">

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="headpost"><h1><?php the_title(); ?></h1></div>

<div class="bodypost">
<div class="projectlist">
<?php $args = array(
	'show_option_all'    => '',
	'orderby'            => 'name',
	'order'              => 'ASC',
	'style'              => 'list',
	'show_count'         => 0,
	'hide_empty'         => 1,
	'use_desc_for_title' => 1,
	'child_of'           => 0,
	'feed'               => '',
	'feed_type'          => '',
	'feed_image'         => '',
	'exclude'            => '',
	'exclude_tree'       => '',
	'include'            => '',
	'hierarchical'       => 1,
	'title_li'           => __( '' ),
	'show_option_none'   => __( 'No categories' ),
	'number'             => null,
	'echo'               => 1,
	'depth'              => 0,
	'current_category'   => 0,
	'pad_counts'         => 0,
	'taxonomy'           => 'category',
	'walker'             => null
); ?>
<?php wp_list_categories( $args ); ?>
</div>
</div>

<?php endwhile; endif; ?>
</div>


<?php include (TEMPLATEPATH . '/sidebar_right.php'); ?>

</div>

</div>
<?php get_footer(); ?>