<?php get_header(); ?>

<div id="content">
<?php
global $options;
foreach ($options as $value) {
	if (get_settings( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_settings( $value['id'] ); }
}
?>
<div class="postsbody">
<div class="allgreen" style="overflow: hidden;margin-bottom: 10px;">
<div class="releases"><h1>Releases <div class="rss"><a href="/feed" target="_blank" title="Feed"><i class="icon-rss"></i></a></div></h1></div>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php include (TEMPLATEPATH . '/'.$fs_post); ?>
<?php endwhile; ?>
</div>
<!-- pagination -->
<div class="pagination">
<div class="right"><?php next_posts_link("Next Post >"); ?></div>
<div class="left"><?php previous_posts_link("< Previous Post"); ?></div>
</div>
<?php else : ?>
<!-- No posts found -->
<?php endif; ?>
</div>

<?php include (TEMPLATEPATH . '/sidebar_right.php'); ?>
</div>
</div>

<?php get_footer(); ?>