<?php get_header(); ?>


<div id="content">
<div class="postsbody">
		<?php if (have_posts()) : ?>
		<div class="releases"><h1>Search Results</h1></div>
<div class="allgreen" style="overflow: hidden;margin-bottom: 10px;">
<?php while (have_posts()) : the_post(); ?>
<?php include (TEMPLATEPATH . '/'.$fs_post); ?>
<?php endwhile; ?>
</div>
<!-- pagination -->
<div class="pagination">
<div class="right"><?php next_posts_link("Next Post >"); ?></div>
<div class="left"><?php previous_posts_link("< Previous Post"); ?></div>
</div>
	<?php else : ?>
		<h2 class="center">No posts found. Try a different search?</h2>
		<center><img src="<?php echo get_template_directory_uri(); ?>/images/404.jpg"/></center>
	<?php endif; ?>
</div>



<?php include (TEMPLATEPATH . '/sidebar_right.php'); ?>

</div>

</div>
<?php get_footer(); ?>