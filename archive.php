<?php get_header(); ?>



<div id="content">



   <div class="postsbody">


	     <?php if (have_posts()) : ?>



              <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>

              <?php /* If this is a category archive */ if (is_category()) { ?>

              <div class="releases" style="margin: 0;"><h1><?php single_cat_title(); ?></h1></div>

              <?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>              

			  <div class="releases"><h1>Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h1></div>

              <?php /* If this is a daily archive */ } elseif (is_day()) { ?>

              <div class="releases" style="margin: 0;"><h1>Archive for <?php the_time('F jS, Y'); ?></h1></div>

              <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>

              <div class="releases" style="margin: 0;"><h1>Archive for <?php the_time('F, Y'); ?></h1></div>

              <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>

              <div class="releases" style="margin: 0;"><h1>Archive for <?php the_time('Y'); ?></h1></div>

              <?php /* If this is an author archive */ } elseif (is_author()) { ?>

              <div class="releases" style="margin: 0;"><h1>Author Archive</h1></div>

              <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>

              <div class="releases" style="margin: 0;"><h1>Blog Archives</h1></div>

              <?php } ?>

<div class="allgreen" style="overflow: hidden;margin-bottom: 10px;">
<?php while (have_posts()) : the_post(); ?>	                            				

                    <div class="postslist">
<div class="left"><h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2> / <?php the_author(); ?> / <?php the_time('F jS, Y'); ?>, <?php the_time('g:i a'); ?></div>
<div class="right"><?php echo $post->comment_count?><?php echo ($post->comment_count==1?' comment':' comments');?></div>
</div>

            <?php endwhile; ?>
</div>
 <!-- pagination -->
<div class="pagination">
<div class="right"><?php next_posts_link("Next Post >"); ?></div>
<div class="left"><?php previous_posts_link("< Previous Post"); ?></div>
</div>

            <?php endif; ?>

              </div>





<?php include (TEMPLATEPATH . '/sidebar_right.php'); ?>



</div>



</div>
<?php get_footer(); ?>
