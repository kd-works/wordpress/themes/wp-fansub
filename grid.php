<div class="gridmode">
<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
<div class="gridimg">
<div class="timeago"><span class="left"><?php the_time('d'); ?></span><span class="right"><?php the_time('M'); ?><br/><?php the_time('Y'); ?></span></div>
<?php if ( has_post_thumbnail() ) { ?> <?php the_post_thumbnail(thumbnail); ?> <?php } else { ?>
<img src="#" /> <?php } ?>
</div>
</a>
<h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
</div>